﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using LitJson;

public class ApiGet : MonoBehaviour
{
    [SerializeField]
    private GameObject space;

    [SerializeField]
    private GameObject scorePrefab;

    public string[] items;

    private List<GameScore> gameScore = new List<GameScore>();

    void Start()
    {
        GetScore();
    }

    /// <summary>
    /// Получение данных рейтинга
    /// </summary>
    private void GetScore()
    {
        try
        {
            string url = "http://admify.projects.ilogos-ua.com/index.php?r=api_v1/GetScore";
            StartCoroutine(RequestGerScore(url));
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }
    /// <summary>
    /// Создание объектов рейтинга
    /// </summary>
    public void ShowScores()
    {
        int index = 0;
        foreach (GameScore score in gameScore)
        {
            GameObject tmpObject = (GameObject)Instantiate(scorePrefab);
            tmpObject.transform.parent = space.transform;
            tmpObject.transform.localScale = Vector3.one;
            tmpObject.GetComponent<ItemScore>().SetItem(++index, score.Name.ToString(), score.Score, score.Level);
            space.GetComponent<UIGrid>().Reposition();

        }
    }
    /// <summary>
    /// Получение рейтинга
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    IEnumerator RequestGerScore(string url)
    {
        WWW data = new WWW(url);

        yield return data;

        string dataString = data.text;

        JsonData itemData = JsonMapper.ToObject(dataString);

        Debug.Log(itemData);
        
        if (itemData != null)
        {
            if (itemData["status"]["code"].ToString() == "200")
            {
                JsonData items = itemData["result"]["items"];
                for (int i = 0; i < items.Count; i++)
                {
                    Debug.Log(items[i][0].ToString());
                    gameScore.Add(new GameScore(
                             Convert.ToInt32(items[i][0].ToString()),
                             items[i][1].ToString(),
                             Convert.ToInt32(items[i][2].ToString()),
                             Convert.ToDateTime(items[i][3].ToString()),
                             Convert.ToInt32(items[i][4].ToString())
                         ));
                }
                gameScore.Sort();
                ShowScores();
            }
            else
            {
                Debug.LogWarning(itemData["status"]["text"].ToString());
            }
        }
        else
        {
            Debug.LogWarning("Ошибка запроса");
        }
    }
}
