﻿using UnityEngine;
using System.Collections;
using System;
using LitJson;

public class ApiSet : MonoBehaviour 
{
    private static ApiSet instance = null;
    /// <summary>
    /// создание сущности
    /// </summary>
    public static ApiSet Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("ApiSet").AddComponent<ApiSet>();
            }
            return instance;
        }
    }
    public void OnApplicationQuit()
    {
        DestroyInstance();
    }

    public void DestroyInstance()
    {
        instance = null;
    }


    /// <summary>
    /// Запись прохождения уровня
    /// </summary>
    /// <param name="score"></param>
    public static void InsertScore(float score)
    {
        try
        {
            string url = String.Format("http://admify.projects.ilogos-ua.com/index.php?r=api_v1/SetScore&name={0}&score={1}&level={2}", Params.username, score, Params.level);
            ApiSet.Instance.StartCoroutine(RequestInsertScore(url));
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }


    /// <summary>
    /// Запись
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    private static IEnumerator RequestInsertScore(string url)
    {
        WWW data = new WWW(url);

        yield return data;

        string dataString = data.text;

        JsonData itemData = JsonMapper.ToObject(dataString);

        if (itemData != null)
        {
            if (itemData["status"]["code"].ToString() == "200")
            {
                Debug.Log(itemData["status"]["text"].ToString());
            }
            else
            {
                Debug.LogWarning(itemData["status"]["text"].ToString());
            }
        }
        else
        {
            Debug.LogWarning("Ошибка запроса");
        }
    }
}
