﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Data;
using Mono.Data.Sqlite;

using UnityEngine.UI;
//Класс БД
public class Db : MonoBehaviour
{
    private string connectionPath;
    private IDbConnection dbConnection;
    private IDbCommand dbCommand;
    private IDataReader reader;

    private static Db instance = null;
    /// <summary>
    /// создание сущности
    /// </summary>
    public static Db Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("Db").AddComponent<Db>();
            }
            return instance;
        }
    }
    public void OnApplicationQuit()
    {
        DestroyInstance();
    }

    public void DestroyInstance()
    {
        instance = null;
    }

    /// <summary>
    /// Инициавлизация
    /// </summary>
    public void Initialize()
    {
        //путь к бд
        connectionPath = "URI=file:" + Application.dataPath + "/snakeDb.sqlite";
        //создание таб.
        string sqlQuery = String.Format("CREATE  TABLE if not exists Score (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , name VARCHAR, score INTEGER, datetime DATETIME DEFAULT CURRENT_DATE, level INTEGER)");

        ActionSql(sqlQuery);
    }
    /// <summary>
    /// Select запросы
    /// после вызвать CloseConnect()
    /// </summary>
    /// <param name="sqlQuery"></param>
    /// <returns></returns>
    public IDataReader SelectSql(string sqlQuery)
    {
        try
        { 
             dbConnection = new SqliteConnection(connectionPath);
            if (dbConnection != null)
            {
                dbConnection.Open();
                dbCommand = dbConnection.CreateCommand();
                if (dbCommand != null)
                {
                    dbCommand.CommandText = sqlQuery;
                    reader = dbCommand.ExecuteReader();
                }
            }
            return reader;
        }      
        catch (Exception exp)
        {
            Debug.LogError(exp);
            return null;
        }
    }
    /// <summary>
    /// Запросы к бд
    /// </summary>
    /// <param name="sqlQuery"></param>
    public void ActionSql(string sqlQuery)
    {
        try
        {
            dbConnection = new SqliteConnection(connectionPath);
            if (dbConnection != null)
            {
                dbConnection.Open();
                dbCommand = dbConnection.CreateCommand();
                if (dbCommand != null)
                {
                    dbCommand.CommandText = sqlQuery;
                    dbCommand.ExecuteScalar();
                    CloseConnect();
                }
            }
        }
        catch (Exception exp)
        {
            Debug.LogError(exp);
        }
    }
    /// <summary>
    /// Закрытие соединения
    /// </summary>
    public void CloseConnect()
    {
        dbCommand.Dispose();
        dbCommand = null;
        dbConnection.Close();
        dbConnection = null;
    }
}
