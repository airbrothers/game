﻿using UnityEngine;
using System.Collections;
//Класс управления камеры 3D
public class Camera3D : MonoBehaviour
{
    #region Fields
    private Player player;
    //цель
    private GameObject target;
    /// <summary>
    /// расстояние от цели
    /// </summary>
    [SerializeField]
    private Vector3 offsetFromTarget = new Vector3(0, -2f, -3f);
    #endregion

    #region Init && Update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");

        SetCameraTarget(target);
    }

    void LateUpdate()
    {
        FollowTarget();
    }
    #endregion

    #region Functions
    /// <summary>
    /// Установка цели
    /// </summary>
    /// <param name="target"></param>
    void SetCameraTarget(GameObject target)
    {
        if (target != null)
        {
            if(target.GetComponent<Player>()){
                player = target.GetComponent<Player>();
            }
            else
            {
                Debug.LogError("Не найден скрипт Player");
            }
        }
        else
        {
            Debug.LogError("Добавьте к камере цель");
        }
    }

    /// <summary>
    /// Слежение за целью
    /// </summary>
    public void FollowTarget()
    {
        transform.position = offsetFromTarget + target.transform.position;
    }
    #endregion
}
