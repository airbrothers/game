﻿using UnityEngine;
using System.Collections;
//Класс переключений камеры
public class SwitchCamera : MonoBehaviour 
{
    [SerializeField]
    private UILabel theText;
    [SerializeField]
    private string text3D;
    [SerializeField]
    private string text2D;
    /// <summary>
    /// Изменение камеры
    /// </summary>
    public void ChangeCamera()
    {
        Params.SwitchCamera();
        if (Params.camera2D)
        {
            theText.text = text2D;
        }
        else if (Params.camera3D)
        {
            theText.text = text3D;
        }
    }
}
