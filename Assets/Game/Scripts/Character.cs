﻿using UnityEngine;
using System.Collections;

public abstract class Character : MonoBehaviour
{
    #region Fields
    //скорость передвижения
    [SerializeField]
    protected float speedMovement = 2f;

    //поворот
    protected Quaternion targetRotation;
    public Quaternion TargetRotation
    {
        get { return targetRotation; }
    }

    //последняя позиция игрока
    protected Vector3 lastPosPlayer;
    //вектор направления игрока
    protected Vector3 direction;
    //позиця игрока
    protected Vector3 characterPos;
    //предыдущие направление движения
    protected Vector3 goDirection;

    //объект аудио
    protected AudioSource  theSource;

    //настройки
    protected GameManager theGM;
    #endregion

    #region Init && Update
    // Use this for initialization
	public virtual void Start () 
    {
        theGM = FindObjectOfType<GameManager>();

        theSource = GetComponent<AudioSource>();

        targetRotation = transform.rotation;
	}
    #endregion

    #region Functions
    /// <summary>
    /// Перемещение героя к позиции
    /// </summary>
    protected void MoveSnake()
    {
        //передвижение героя
        transform.position = Vector3.MoveTowards(transform.position, characterPos, Time.deltaTime * speedMovement);
    }
    /// <summary>
    /// Воспроизведение аудио
    /// </summary>
    /// <param name="audio"></param>
    protected void PlaySound(AudioClip audio)
    {
        if (audio != null)
        {
            theSource.clip = audio;
            theSource.Play();
        }
        else
        {
            Debug.LogWarning("Нет звука");
        }
    }
    #endregion
}
