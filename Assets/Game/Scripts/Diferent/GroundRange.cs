﻿using UnityEngine;
using System.Collections;
//Класс выбора изображения поверхности
public class GroundRange : MonoBehaviour 
{
    //массив текстур
    Texture[] texture;

	void Start () 
    {
        //загрузка текстур
        texture = Resources.LoadAll<Texture>("Ground");

        SetTexture();

        SetGround();
	}
    /// <summary>
    /// Установка текстуры
    /// </summary>
    private void SetTexture()
    {
        gameObject.GetComponent<Renderer>().material.mainTexture = texture[Random.Range(0, texture.Length)];
    }
    /// <summary>
    /// Установка background'a
    /// </summary>
    private void SetGround()
    {
        if (Params.camera2D)
        {
            gameObject.transform.localScale = new Vector3(2f, 2f, 2f);
        }
        else if (Params.camera3D)
        {
            gameObject.transform.localScale = new Vector3(3f, 3f, 3f);
        }
       
    }
}
