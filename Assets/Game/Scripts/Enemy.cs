﻿using UnityEngine;
using System.Collections;
//Класс самолетов
public class Enemy : Character, IMove
{
    #region Fields
    //цель
    private GameObject player;
    //дистанция до цели
    [SerializeField]
    private float distance;
    //звук приближения
    public AudioClip enemyAudio;

    [SerializeField]
    private float timeDestroy = 20f;
    private float time;
    #endregion

    #region Check Fields
    /// <summary>
    /// Определение дистанция до цели
    /// </summary>
    private bool PlayerCheck
    {
        get
        {
            if (player != null)
            {
                return Vector3.Distance(player.transform.position, transform.position) < distance;
            }
            return false;
        }

    }
    /// <summary>
    /// Определение стороны
    /// </summary>
    private bool PlayerSideCheck
    {
        get
        {
            if (player != null)
            {
                Vector3 pos = transform.InverseTransformPoint(player.transform.position);
                if (pos.y > 0)
                {
                    return true;
                }
            }
            return false;
        }

    }
    #endregion

    #region Init && Update
    /// <summary>
    /// При старте
    /// </summary>
	void Start () 
    {
        base.Start();
        //Определение позиции на карте
        GetEnemy();
        //позция игрока
        transform.position = characterPos;
        //объект игрока
        player = GameObject.FindGameObjectWithTag("Player");
	}
	/// <summary>
    /// Вызывается каждый кадр
	/// </summary>
	void Update () 
    {
        //Передвижение
        Move();
        //Воспроизведение звука если объект находится в зоне
        if (PlayerCheck && PlayerSideCheck && !theSource.isPlaying && !Params.IsActivePopup())
        {
            PlaySound(enemyAudio);
        }
        //уничтожение объекта
        CheckDestroy();
	}
    #endregion

    #region Functions
    /// <summary>
    /// Передвижение героя
    /// </summary>
    public void Move()
    {
        //точка поворота по сетке
        if (transform.position == characterPos)
        {
            //новая поцзиция героя
            characterPos += direction;
            //поворот героя
            transform.rotation = targetRotation;
        }

        base.MoveSnake();    
    }
    /// <summary>
    /// Позиция на карте
    /// </summary>
    private void GetEnemy()
    {
        //определение позиции
        int position = Random.Range(0, theGM.theMap.sizeMap);
        //определение стороны
        int size = Random.Range(1, 4);
        //cправо на карте
        if (size == 1)
        {
            direction = Vector3.right;
            targetRotation.eulerAngles = new Vector3(0, 0, -90);
            characterPos = new Vector3(-2, position);
        }
        //слева на карте
        if (size == 2)
        {
            direction = Vector3.left;
            targetRotation.eulerAngles = new Vector3(0, 0, 90);
            characterPos = new Vector3(theGM.theMap.sizeMap + 2, position);
        }
        //вверху на карты
        if (size == 3)
        {
            direction = Vector3.up;
            targetRotation.eulerAngles = new Vector3(0, 0, 0);
            characterPos = new Vector3(position, -2);
        }
        //внизу на карты
        if (size == 4)
        {
            direction = Vector3.down;
            targetRotation.eulerAngles = new Vector3(0, 0, 180);
            characterPos = new Vector3(position, theGM.theMap.sizeMap + 2);
        }
    }
    /// <summary>
    /// Уничтожение после выходы из поля
    /// </summary>
    void CheckDestroy()
    {
        time += Time.deltaTime;
        if (time > timeDestroy)
        {
            Destroy(gameObject);
        }
    }
    #endregion
}
