﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//Класс менеджер игры
public class GameManager : MonoBehaviour, IGameManager
{
    #region Fields
    //количество жизней
    [SerializeField]
    private int live = 3;
    //количество элементов кот. нужно найти
    [SerializeField]
    private int search = 5;

    [SerializeField]
    private float respownBonusDelay = 10f;
    private float respownBonusTime;

    [SerializeField]
    private float respownEnemyDelay = 6f;
    private float respownEnemyTime;

    [SerializeField]
    private GameObject foodPerfab;

    [SerializeField]
    private GameObject[] bonus;

    [SerializeField]
    private GameObject camera3D;
    [SerializeField]
    private GameObject camera2D;

    //враги
    public GameObject[] enemies;

    [SerializeField]
    private float flashTimeDelay = 5f;
    private bool flash;

    //Фразы
    [SerializeField]
    private float timePeraphDelay = 2f;
    private float timePeraph;

    private GameObject peraphObj;

    public string[] peraph;

    public List<Vector3> iseetObj;

    private bool activeLose, activeWin, activePause = false;

    [HideInInspector]
    public Map theMap;
    [HideInInspector]
    public SearchManager theSearch;
    [HideInInspector]
    public LifeManager theLive;
    [HideInInspector]
    public ScoreManager theScore;
    [HideInInspector]
    public TimeManager theTime;
    [HideInInspector]
    public FuelManager theFuel;

    [HideInInspector]
    public LosePopup theLose;
    [HideInInspector]
    public WinPopup theWin;
    [HideInInspector]
    public PausePopup thePause;
    #endregion

    #region Init && Update
    /// <summary>
	/// Инициализация парметров
    /// Генерация карты
    /// Настройка камеры
	/// </summary>
    void Awake() 
    {
        PlayerPrefs.SetInt("playerLives", live);

        theMap = FindObjectOfType<Map>();
        if (theMap != null)
        {
            //Генерация карты
            theMap.Initialize();  
            //Настройка камеры
            CameraInit();
            //Инициализация попав
            theLose = FindObjectOfType<LosePopup>();
            theWin = FindObjectOfType<WinPopup>();
            thePause = FindObjectOfType<PausePopup>();
            //Инициализация менеджеров
            theLive = FindObjectOfType<LifeManager>();
            theScore = FindObjectOfType<ScoreManager>();
            theSearch = FindObjectOfType<SearchManager>();
            theFuel = FindObjectOfType<FuelManager>();
            theTime = FindObjectOfType<TimeManager>();
            //Установка кол. элеменов поиска
            theSearch.CountItem = search;
            //Создание еды
            if (foodPerfab != null)
            {
                CreateFood();
            }
            else
            {
                Debug.LogWarning("Не добавлен объект еды");
            }
            //Вывод надписи
            peraphObj = GameObject.FindGameObjectWithTag("Peraph");
            SetPeraph(0);
        }
        else
        {
            Debug.LogWarning("Карта не иницаализирована");
        }
	}

    /// <summary>
    /// Вызывается каждый кадр
    /// </summary>
    void Update()
    {
        if (!theSearch.FullSearch)
        {
            //создание самолетов
            CreateEnemies();
            //создание бонусов
            CreateBounus();
        }
        //Активация попапа проигрыша
        if (Params.lose && !activeLose)
        {
            activeLose = !activeLose;
            theLose.Initialize();
        }
        //Активация попапа выигрыша
        if (Params.win && !activeWin)
        {
            activeWin = !activeWin;
            theWin.Initialize();
        }
        //Активация попапа паузы
        if (Params.pause && !activePause)
        {
            activeWin = !activeWin;
            thePause.Initialize();
        }
    }
    #endregion

    #region Generate functions
    /// <summary>
    /// Создание самолетов
    /// </summary>
    public void CreateEnemies()
    {
        //создание врага
        respownEnemyTime += Time.deltaTime;
        if (respownEnemyTime > respownEnemyDelay)
        {
            Instantiate(enemies[Random.Range(0, enemies.Length)]);
            respownEnemyTime = 0;
        }
    }
    /// <summary>
    /// Создание еды
    /// </summary>
    public void CreateFood()
    {
        GameObject ceil = GeneratePositions();

        Vector3 posBonus = ceil.transform.position + Vector3.back;

        if (!iseetObj.Contains(posBonus))
        {
            StartCoroutine(GenerateBonuses(ceil));
            Instantiate(foodPerfab, posBonus, Quaternion.identity);
            iseetObj.Add(posBonus);
        }
        else
        {
            CreateFood();
        }
    }

    /// <summary>
    /// Создание бонусов
    /// </summary>
    public void CreateBounus()
    {
        respownBonusTime += Time.deltaTime;
        if (respownBonusTime > respownBonusDelay)
        {
            GameObject ceil = GeneratePositions();

            Vector3 posBonus = ceil.transform.position + Vector3.back;

            if (!iseetObj.Contains(posBonus))
            {
                StartCoroutine(GenerateBonuses(ceil));
                Instantiate(bonus[Random.Range(0, bonus.Length)], posBonus, Quaternion.identity);
                iseetObj.Add(posBonus);
                respownBonusTime = 0;
            }
            else
            {
                CreateBounus();
            }
        }
    }
    /// <summary>
    /// Опредеоение позиций
    /// </summary>
    /// <returns></returns>
    private GameObject GeneratePositions()
    {
        int ranX = Random.Range(0, theMap.sizeMap);
        int ranY = Random.Range(0, theMap.sizeMap);
        return theMap._mapArea[ranX, ranY];
    }

    /// <summary>
    /// Выполнение генерации бонуса
    /// </summary>
    /// <param name="map"></param>
    /// <returns></returns>
    IEnumerator GenerateBonuses(GameObject map)
    {
        Renderer render = map.GetComponent<Renderer>();
        render.material.shader = Shader.Find("Transparent/Diffuse");
        render.material.color = new Color(0, 1, 0, 0.7f);

        flash = true;
        StartCoroutine(FlashPartMap(render));
        yield return new WaitForSeconds(flashTimeDelay);
        flash = false;

        yield return null;
    }
    /// <summary>
    /// Мерцание под объектом
    /// </summary>
    /// <param name="render"></param>
    /// <returns></returns>
    IEnumerator FlashPartMap(Renderer render)
    {
        while (flash)
        {
            yield return new WaitForSeconds(.1f);
            render.enabled = true;
            yield return new WaitForSeconds(.1f);
            render.enabled = false;
        }

        yield return null;
    }
    #endregion

    #region Peraph functions
    /// <summary>
    /// Вывод надписи
    /// </summary>
    /// <param name="index"></param>
    public void SetPeraph(int index)
    {
        StartCoroutine(StartPeraph(index));
    }
    /// <summary>
    /// Показываем/Убираем надписи
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    private IEnumerator StartPeraph(int index)
    {
        NGUITools.SetActive(peraphObj, true);
        peraphObj.GetComponent<UILabel>().text = peraph[index];
        yield return new WaitForSeconds(timePeraphDelay);
        NGUITools.SetActive(peraphObj, false);
        yield return null;
    }
    #endregion

    #region Functions
    /// <summary>
    /// Разблокировка игры
    /// </summary>
    public void UnlockGame()
    {
        int openLevel = Params.GetNextLevel();
        if (openLevel != 0)
        {
            PlayerPrefs.SetInt("Level " + openLevel, 1);
        }
    }
    /// <summary>
    /// Настройка камеры
    /// </summary>
    private void CameraInit()
    {
        if (Params.camera2D)
        {
            camera2D.SetActive(true);
            camera3D.SetActive(false);
        }
        else if (Params.camera3D)
        {
            camera2D.SetActive(false);
            camera3D.SetActive(true);
        }
    }
    /// <summary>
    /// После закрытия
    /// </summary>
    void OnDestroy()
    {
        //Отчистка параметров
        Params.Reset();
    }
    #endregion
}
