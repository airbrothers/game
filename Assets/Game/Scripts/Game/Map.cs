﻿using UnityEngine;
using System.Collections;
//Класс карты
public class Map : MonoBehaviour, IMap
{
    #region Fields
    //размер карты
    public int sizeMap;
    //объект клетки
    [SerializeField]
    private GameObject _mapCeil;
    //массив сетки
    public GameObject[,] _mapArea;
    //массив стенок
    private GameObject[] _borderArea = new GameObject[4];
    [SerializeField]
    private GameObject _borderMap;

    private float offset = 0.5f;
    #endregion

    #region Init
    //Инициализация
    public void Initialize()
    {
        GenerateMap();
        GenerateBorder();
    }
    #endregion

    #region Functions
    /// <summary>
    /// Генерация карты
    /// </summary>
    private void GenerateMap()
    {
        _mapArea = new GameObject[sizeMap, sizeMap];
        for (int x = 0; x < sizeMap; x++)
        {
            for (int y = 0; y < sizeMap; y++)
            {
                _mapArea[x, y] = (GameObject)Instantiate(_mapCeil, new Vector3(x, y,gameObject.transform.position.z), Quaternion.identity);
                _mapArea[x, y].transform.SetParent(gameObject.transform);
            }
        }
    }

    /// <summary>
    /// Генерация стенок
    /// </summary>
    private void GenerateBorder()
    {
        float size = sizeMap;
        Texture border = Resources.Load<Texture>("border");
       
        for (int i = 0; i < 4; i++)
        {
            _borderArea[i] = Instantiate(_borderMap);

            float borderY = (Params.camera3D) ? 0.01f : 0.1f;

            _borderArea[i].transform.localScale = new Vector3(size, borderY, 1);

            if(i >= 2){
                _borderArea[i].transform.localEulerAngles = new Vector3(0, 0, 180);
            }

            if (border != null)
            {
                _borderArea[i].GetComponent<Renderer>().material.mainTexture = border;
                _borderArea[i].GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
            }
            
            _borderArea[i].transform.SetParent(gameObject.transform);
        }
        //left
        _borderArea[0].transform.position = new Vector3(-offset, size / 2 - offset, 0);
        //right
        _borderArea[1].transform.position = new Vector3(size - offset, size / 2 - offset, 0);
        //bottom
        _borderArea[2].transform.position = new Vector3(size / 2 - offset, -offset, 0);
        //top
        _borderArea[3].transform.position = new Vector3(size / 2 - offset, size - offset, 0);
    }
    #endregion
}
