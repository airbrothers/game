﻿using UnityEngine;
using System.Collections;

public abstract class BaseManager : MonoBehaviour
{
    private UILabel theText;

	public virtual void Start () 
    {
        theText = GetComponent<UILabel>();
	}

    protected void SetText(string text)
    {
        theText.text = text;
    }
}
