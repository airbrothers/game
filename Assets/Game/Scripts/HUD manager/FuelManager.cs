﻿using UnityEngine;
using System.Collections;
//Класс контроля топлива
public class FuelManager : BaseManager
{
    #region Fields
    //количество бака
    [SerializeField]
    private int tank = 100;
    public int Tank
    {
        set { tank = value; }
    }
    //время использования бака
    [SerializeField]
    private int timeFull = 30;

    private TimeManager theTime;

    private int startTank, nowTank;
    private int startTimeFull;
    #endregion

    #region Init && Update
    /// <summary>
    /// Use this for initialization
    /// </summary>
	public override void Start () 
    {
        base.Start();

        theTime = FindObjectOfType<TimeManager>();
    
        startTank = nowTank = tank;
        startTimeFull = timeFull;
	}

    /// <summary>
    /// Update is called once per frame
    /// </summary>
	void Update () 
    {
        float count = theTime.GameTime * tank / timeFull;

        if (count < nowTank)
        {
            SetText(nowTank - Mathf.Round(count) + "%");
        }
        else
        {
            Params.lose = true;
        }
	}
    #endregion

    #region Functions
    /// <summary>
    /// Добавление керосина
    /// </summary>
    public void AddFuel()
    {
        nowTank += startTank / 3;
    }
    /// <summary>
    /// Сброс данных
    /// </summary>
    public void Reset()
    {
        tank = nowTank = startTank;
        timeFull = startTimeFull;
    }
    #endregion
}
