﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//Класс контроля жизни
public class LifeManager : BaseManager
{
    #region Fields
    //колличество жизней
    private int lifeCounter;

    //возврат значение смерти или повреждения
    public bool IsDead
    {
        get {
            if (lifeCounter <= 0)
            {
                Params.lose = true;
            }
            return lifeCounter <= 0; 
        }
    }
    #endregion

    #region Init && Upadte
    public override void Start () 
    {
        base.Start();
        lifeCounter = PlayerPrefs.GetInt("playerLives");
	}
	
	void Update () 
    {
        SetText("x " + lifeCounter);
	}
    #endregion

    #region Functions
    /// <summary>
    /// Добавляем жизнь
    /// </summary>
    public void AddLife()
    {
        lifeCounter++;
        PlayerPrefs.SetInt("playerLives", lifeCounter);
    }
    /// <summary>
    /// Отнимаем жизнь
    /// </summary>
    public void TakeLife()
    {
        lifeCounter--;
        PlayerPrefs.SetInt("playerLives", lifeCounter);
    }
    #endregion
}
