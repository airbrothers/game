﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//Класс контроля очков
public class ScoreManager : BaseManager
{
    #region Fields
    //количество очков
    [SerializeField]
    private float score;
    public float Score
    {
        get { return score; }
    }
    #endregion

    #region Init && Update
    /// <summary>
    /// Use this for initialization
    /// </summary>
	void Start () 
    {
        base.Start();
	}
    /// <summary>
    /// Update is called once per frame
    /// </summary>
	void Update () 
    {
        SetText(score.ToString());
	}
    #endregion

    #region Functions
    /// <summary>
    /// Добавление очков
    /// </summary>
    /// <param name="point"></param>
    public void AddScore(float point)
    {
        score += point;
    }
    /// <summary>
    /// Удаление очков
    /// </summary>
    /// <param name="point"></param>
    public void DeleteScore(float point)
    {
        score -= point;
    }
    /// <summary>
    /// Сброс данных
    /// </summary>
    public void Reset()
    {
        score = 0;
    }
    #endregion
}
