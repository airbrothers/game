﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//Класс контроля поиска элементов
public class SearchManager : BaseManager
{
    #region Fields
    //сколько нужно найти
    private int countItem;

    public int CountItem
    {
        set { countItem = value; }
    }
    //сколько найдено
    private int searchItem;

    public int SearchItem
    {
        get { return searchItem; }
        set { searchItem = value; }
    }

    //возврат соотношения 
    public bool FullSearch
    {
        get
        {
            return searchItem + 1 == countItem;
        }
    }
    #endregion

    #region Init && Update
    void Start () 
    {
        base.Start();
	}

	void Update () 
    {
        SetText(searchItem + "/" + countItem);
        //уровень пройден
        if (searchItem == countItem)
        {
            Params.win = true;
        }
	}
    #endregion

    #region Functions
    /// <summary>
    /// Добавление итема
    /// </summary>
    /// <param name="item"></param>
    public void Add(int item)
    {
        searchItem += item;
    }
    #endregion
}
