﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//Класс контроля времени
public class TimeManager : BaseManager
{
    #region Fields
    //время от инициализации
    private float gameTime;

    public float GameTime
    {
        get { return gameTime; }
    }
    #endregion

    #region Init && Update
    /// <summary>
    /// Use this for initialization
    /// </summary>
	void Start () 
    {
        base.Start();
	}

    /// <summary>
    /// Update is called once per frame
    /// </summary>
	void Update () 
    {
        gameTime+=Time.deltaTime;
        SetText(Mathf.Round(gameTime).ToString());
	}
    #endregion

    #region Functions
    /// <summary>
    /// Сброс данных
    /// </summary>
    public void Reset()
    {
        gameTime = 0;
    }
    #endregion
}
