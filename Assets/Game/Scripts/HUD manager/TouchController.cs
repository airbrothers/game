﻿using UnityEngine;
using System.Collections;
//Класс контроля тач
public class TouchController : MonoBehaviour 
{
    //главный герой
    private Player thePlayer;

	void Start () 
    {
        thePlayer = FindObjectOfType<Player>();
	}
    /// <summary>
    /// Поворот влево
    /// </summary>
    public void RightArrow()
    {
        thePlayer.HandleMovement(1, 0);
    }
    /// <summary>
    /// Поворот вправо
    /// </summary>
	public void LeftArrow() 
    {
        thePlayer.HandleMovement(-1, 0);
	}
    /// <summary>
    /// Поворот вверх
    /// </summary>
    public void UpArrow()
    {
        thePlayer.HandleMovement(0, 1);
    }
    /// <summary>
    /// Поворот вниз
    /// </summary>
    public void DownArrow()
    {
        thePlayer.HandleMovement(0, -1);
    }
    /// <summary>
    /// Пауза
    /// </summary>
    public void Pause()
    {
        Params.pause = true;
    }
}
