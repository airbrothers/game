﻿using UnityEngine;
using System.Collections;

interface IGameManager {

    void CreateFood();

    void CreateBounus();

    void CreateEnemies();
}
