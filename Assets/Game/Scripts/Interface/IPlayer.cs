﻿using UnityEngine;
using System.Collections;

interface IPlayer{

    void HandleMovement(float h, float v);

    void AddTail();

    void OnTriggerEnter(Collider collider);
}
