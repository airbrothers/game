﻿using UnityEngine;
using System.Collections;

interface IPopup 
{
    void Initialize();

    void GetInfo();

    void activePopup(GameObject obj);
}
