﻿using UnityEngine;
using System.Collections;
//Класс бонусов для змейки
public class Bonus : MonoBehaviour
{
    #region Fields
    //кол. очков
    [SerializeField]
    private int point;
    //скорость поворота
    [SerializeField]
    private float moveSpeed = 2f;
    //время удаления
    [SerializeField]
    private float bonusDelete = 10f;
    //время после инициализации
    private float timeShow;

    private GameManager theGM;
    #endregion

    #region Init && Update
    /// <summary>
    /// Инициализация
    /// </summary>
    void Start()
    {
        theGM = FindObjectOfType<GameManager>();
    }
    /// <summary>
    /// Вызывается каждый кадр
    /// </summary>
	void Update () {
        //удаление и отчистка
        timeShow += Time.deltaTime;
        if (timeShow > bonusDelete)
        {
            theGM.iseetObj.Remove(gameObject.transform.position);
            Destroy(gameObject);
        }
        //повороты объекта
        transform.Rotate(new Vector3(0, 0, Time.deltaTime * moveSpeed));  
	}
    #endregion

    #region Functions
    /// <summary>
    /// Добавление бонуса
    /// </summary>
    public void AddBonus()
    {
        theGM.theScore.AddScore(point);
        if (gameObject.tag == "Live")
        {
            theGM.theLive.AddLife();
        }
        else if (gameObject.tag == "Tank")
        {
            theGM.theFuel.AddFuel();
        }
    }
    #endregion
}
