﻿using UnityEngine;
using System.Collections;
//Класс еды для змейки
public class Food : MonoBehaviour 
{
    //кол. очков
    [SerializeField]
    private int point;

    public int Point
    {
        get { return point; }
    }
    //скорость поворота
    [SerializeField]
    private float moveSpeed = 2f;

    /// <summary>
    /// Повороты объекта
    /// </summary>
	void Update () {
        transform.Rotate(new Vector3(0, 0, Time.deltaTime * moveSpeed));  
	}

}
