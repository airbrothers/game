﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//Класс игрока
public class Player : Character, IPlayer, IMove
{
    #region Fields
    [SerializeField]
    private int health;
    //Объект хвоста
    [SerializeField]
    private GameObject tailPrefab;
    //возможное количество хвоста
    [SerializeField]
    private int countItem;
    //позиция добавления части хвоста
    private Vector3 addTailPos;
    //стартовая скорость
    private float startSpeed;
    //стартовая позиция
    private Vector3 startPosition;
    //хвост героя
    private List<Transform> tail = new List<Transform>();
    //позиция хвоста героя
    private Vector3[] nextBodyPos; 
    //звук еды
    public AudioClip foodAudio;
    //бессмертие
    [SerializeField]
    private float imortalTime;
    private bool importal = false;
    //потомок объекта
    private Transform childPlane;
    #endregion

    #region Init && Update
    public override void Start()
    {
        base.Start();

        //позция игрока
        transform.position = new Vector3(theGM.theMap.sizeMap / 2, theGM.theMap.sizeMap / 2);

        startPosition = characterPos = transform.position;
        //массив с координатами хвоста
        nextBodyPos = new Vector3[countItem];
        //потомок объекта
        childPlane = gameObject.transform.GetChild(0);

        startSpeed = speedMovement;
    }

    void Update()
    {
        #if UNITY_EDITOR  ||  UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_WEBGL
        //Движение по оси
            HandleMovement(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        #endif

        //Перемещение
        Move();

        CheckBorder();
    }
    #endregion

    #region Movement functions
    /// <summary>
    /// Векторы поворота героя
    /// </summary>
    /// <param name="horizontal"></param>
    /// <param name="vertical"></param>
    public void HandleMovement(float horizontal, float vertical)
    {
        if (horizontal > 0 && goDirection != Vector3.left)
        {
            direction = Vector3.right;
            targetRotation.eulerAngles = new Vector3(0, 0, -90);
        }
        if (horizontal < 0 && goDirection != Vector3.right)
        {
            direction = Vector3.left;
            targetRotation.eulerAngles = new Vector3(0, 0, 90);
        }
        if (vertical > 0 && goDirection != Vector3.down)
        {
            direction = Vector3.up;
            targetRotation.eulerAngles = new Vector3(0, 0, 0);
        }
        if (vertical < 0 && goDirection != Vector3.up)
        {
            direction = Vector3.down;
            targetRotation.eulerAngles = new Vector3(0, 0, 180);
        }
    }

    /// <summary>
    /// Передвижение героя
    /// </summary>
    public void Move()
    {
        //точка поворота по сетке
        if (transform.position == characterPos)
        {
            //текущая позиция героя
            lastPosPlayer = characterPos;
            //направление движения
            goDirection = direction;
            //новая поцзиция героя
            characterPos += direction;
            //поворот героя
            transform.rotation = targetRotation;
            //позиция хвоста
            for (int i = 0; i < tail.Count; i++)
            {
                nextBodyPos[i] = tail[i].position;
            } 
        }

        base.MoveSnake();

        //передвижение хвоста
        for (int i = 0; i < tail.Count; i++)
        {
            //начало ховста к голове
            if (i == 0)
            {
                tail[i].position = Vector3.MoveTowards(tail[i].position, lastPosPlayer, Time.deltaTime * speedMovement);
            }
            //остальная часть хвоста
            else
            {
                tail[i].position = Vector3.MoveTowards(tail[i].position, nextBodyPos[i - 1], Time.deltaTime * speedMovement);
            }
            //направление к герою
            tail[i].LookAt(characterPos);
        }
    }
    #endregion

    #region Functions
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Food") //при подборе еды
        {
            //аудио
            PlaySound(foodAudio);
            //надпись
            theGM.SetPeraph(1);
            //увелечение скорости
            speedMovement += 0.1f;
            //Уничтожение объекта
            Destroy(other.gameObject);
            //добавление хвоста
            AddTail();
            //Создание еды
            if (!theGM.theSearch.FullSearch)
            {
                theGM.CreateFood();
            }
            theGM.theSearch.Add(1);
            //Добвление очков
            theGM.theScore.AddScore(other.gameObject.GetComponent<Food>().Point);
            //удаление из списка
            theGM.iseetObj.Remove(other.gameObject.transform.position);
        }
        else if (other.tag == "Live" || other.tag == "Tank") //при подборе бонусов
        {
            //аудио
            PlaySound(foodAudio);
            //уничтожение
            Destroy(other.gameObject);
            //добавление очков
            other.GetComponent<Bonus>().AddBonus();
            //удаление из массива позиций
            theGM.iseetObj.Remove(other.gameObject.transform.position);
            //надпись
            theGM.SetPeraph(1);
        }
        else if (other.tag == "Tail" || other.tag == "Enemy") //при сопрекосновении с хвостом и другим самолетом
        {
            //надпись
            theGM.SetPeraph(2);
            //уничтожение
            StartCoroutine(TakeDamage());
        }
    }
    /// <summary>
    /// Мигание игрока
    /// </summary>
    /// <returns></returns>
    public IEnumerator FlashPlayer()
    {
        while (importal)
        {
            childPlane.gameObject.SetActive(false);
            yield return new WaitForSeconds(.1f);
            childPlane.gameObject.SetActive(true);
            yield return new WaitForSeconds(.1f);
        }
    }
    /// <summary>
    /// При уничтожении
    /// </summary>
    /// <returns></returns>
    public IEnumerator TakeDamage()
    {
        if (!importal)
        {
            ClearSearch();

            if (!theGM.theLive.IsDead)
            {
                importal = true;
                StartCoroutine(FlashPlayer());
                yield return new WaitForSeconds(imortalTime);
                importal = false;
            }
            yield return null;
        }
    }
    /// <summary>
    /// Добавление частей хвоста
    /// </summary>
    public void AddTail()
    {
        if (tail.Count > 0)
        {
            addTailPos = tail[tail.Count - 1].position;
        }
        else
        {
            addTailPos = lastPosPlayer;
        }

        Transform tailObj = (Transform)((GameObject)Instantiate(tailPrefab, addTailPos, Quaternion.identity)).transform;
        tail.Add(tailObj);
    }
    /// <summary>
    /// Проверка выхода за пределы карты
    /// </summary>
    private void CheckBorder()
    {
        if (transform.position.x < 0 || transform.position.x > theGM.theMap.sizeMap - 1
            || transform.position.y < 0 || transform.position.y > theGM.theMap.sizeMap - 1
            )
        {
            theGM.SetPeraph(3);
            StartCoroutine(TakeDamage());
        }
    }
    #endregion

    #region Reset functions
    /// <summary>
    /// Сброс данных полсе гибели
    /// </summary>
    private void ClearSearch()
    {
        speedMovement = startSpeed;

        theGM.theLive.TakeLife();
        theGM.theFuel.Reset();
        theGM.theScore.Reset();
        theGM.theTime.Reset();
        theGM.theSearch.SearchItem = 0;

        transform.position = characterPos = startPosition;

        GameObject [] tailObjs = GameObject.FindGameObjectsWithTag("Tail");
        foreach(GameObject item in tailObjs){
            Destroy(item.gameObject);
        }
      
        nextBodyPos = new Vector3[countItem];

        tail.Clear();
    }
    #endregion
}
