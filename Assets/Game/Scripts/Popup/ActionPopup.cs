﻿using UnityEngine;
using System.Collections;
//Класс действий из попапа
public class ActionPopup : MonoBehaviour
{
    //название сцены главного меню
    [SerializeField]
    private string mainMenu;
    //объект менеджера
    [SerializeField]
    private GameObject manager;

    /// <summary>
    /// Главное меню
    /// </summary>
    public void MainMenu()
    {
        Params.menu = 3;  
        LoadScene(mainMenu); 
    }
    /// <summary>
    /// Выбор уровня
    /// </summary>
    public void LevelSelect()
    {
        Params.menu = 1;
        LoadScene(mainMenu);
    }
    /// <summary>
    /// Рейтинг
    /// </summary>
    public void Rating()
    {
        Params.menu = 2;
        LoadScene(mainMenu);
    }
    /// <summary>
    /// Выход
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }
    /// <summary>
    /// Перезагрузка
    /// </summary>
    public void Reload()
    {
        ClosePopup();
        Time.timeScale = 1f;
        Params.Reset();
        Application.LoadLevel(Application.loadedLevel);
    }
    /// <summary>
    /// Возврат
    /// </summary>
    public void Return()
    {
        ClosePopup();
        NGUITools.SetActive(manager, true);
        Params.Reset();
        Time.timeScale = 1f;
    }
    /// <summary>
    /// загрузка сцены
    /// </summary>
    /// <param name="scene"></param>
    private void LoadScene(string scene)
    {
        ClosePopup();
        Time.timeScale = 1f;
        Application.LoadLevel(scene);
    }
    /// <summary>
    /// Закрытие попапов
    /// </summary>
    private void ClosePopup()
    {
        //use free version NGUI
        GameObject[] items = GameObject.FindGameObjectsWithTag("Menu");

        foreach (GameObject item in items)
        {
            NGUITools.SetActive(item, false);
        }
    }
}
