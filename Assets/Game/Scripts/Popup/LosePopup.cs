﻿using UnityEngine;
using System.Collections;
//Попап проигрыша
public class LosePopup : Popup 
{
    /// <summary>
    /// Объект попапа
    /// </summary>
    [SerializeField]
    private GameObject canvasLose;
    /// <summary>
    /// Инициализация
    /// </summary>
    public override void Initialize()
    {
        base.Initialize();

        base.GetInfo();

        base.activePopup(canvasLose);
    }
}
