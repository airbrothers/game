﻿using UnityEngine;
using System.Collections;
//Попап паузы
public class PausePopup : Popup
{    
    /// <summary>
    /// Объект попапа
    /// </summary>
    [SerializeField]
    private GameObject canvasPause;
    /// <summary>
    /// Инициализация
    /// </summary>
    public override void Initialize()
    {
        base.Initialize();

        base.GetInfo();

        base.activePopup(canvasPause);
    }
}
