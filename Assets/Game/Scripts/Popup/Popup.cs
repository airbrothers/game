﻿using UnityEngine;
using System.Collections;
//Класс базовый попапов
public class Popup : MonoBehaviour, IPopup
{
    [SerializeField]
    private UILabel textScore;

    [SerializeField]
    private UILabel textSearch;

    [SerializeField]
    private UILabel textTime;

    protected GameManager theGM;

    public virtual void Initialize()
    {
        theGM = FindObjectOfType<GameManager>();
	}

    public void GetInfo()
    {

        textScore.text = theGM.theScore.Score.ToString();

        textSearch.text = theGM.theSearch.SearchItem.ToString();

        textTime.text = Mathf.Round(theGM.theTime.GameTime).ToString();

    }
    /// <summary>
    /// Активация попапов
    /// </summary>
    /// <param name="obj"></param>
    public void activePopup(GameObject obj)
    {
        NGUITools.SetActive(obj, true);
        Time.timeScale = 0f;
    }
}
