﻿using UnityEngine;
using System.Collections;
//Класс попап выигрыша
public class WinPopup : Popup 
{
    /// <summary>
    /// Объект попапа
    /// </summary
    [SerializeField]
    private GameObject canvasWin;
    [SerializeField]
    private float completedDelay = 2f;
    /// <summary>
    /// Инициализация
    /// </summary>
    public override void Initialize()
    {
        base.Initialize();

        base.GetInfo();
        //старт задержки
        StartCoroutine(Wait());
        //добавление очков
        ApiSet.InsertScore(theGM.theScore.Score);
        //RatingDb.Add(theGM.theScore.Score);

        //разблокировка следующиго уровня
        theGM.UnlockGame();
    }
    /// <summary>
    /// Задержка при открытии
    /// </summary>
    /// <returns></returns>
    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(completedDelay);
        base.activePopup(canvasWin);
        yield return null;
    }
}
