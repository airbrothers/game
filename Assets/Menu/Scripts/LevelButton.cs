﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//Класс кнопки
public class LevelButton : MonoBehaviour
{
    //текст
    public string levelText;
    //разблокировка
    public int unlocked;
    //уровень
    public int level;
    /// <summary>
    /// Переход на уровень
    /// </summary>
    public void GoToLevel()
    {
        if (unlocked == 1)
        {
            Params.level = level;

            Application.LoadLevel(levelText);
        }
    }
}
