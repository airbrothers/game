﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
//Класс уровней
public class LevelLoad : MonoBehaviour
{
    //уровни
    [System.Serializable]
    public class Level
    {
        public string nameLevel;
        public int levelNum;
        public string levelText;
        public int unLocked;
    }
    public List<Level> levelList;

    //Текст разблокировки
    [SerializeField]
    private string unlockText;
    //Текст блокировки
    [SerializeField]
    private string lockText;
    //Объект блока уровня
    public GameObject block;
    //блок уровней
    public Transform space;

    void Start()
    {
        AddLevel();
    }
    /// <summary>
    /// Добавление уровней
    /// </summary>
    private void AddLevel()
    {
        Params.maxLevel = levelList.Count;
        foreach (Level list in levelList)
        {
            //создание объекта
            GameObject tmpObject = (GameObject)Instantiate(block);
            tmpObject.transform.parent = space.transform;
            tmpObject.transform.localScale = Vector3.one;
            tmpObject.transform.GetChild(1).GetComponent<UILabel>().text = list.levelText;
            //разблокировка уровней
            if (PlayerPrefs.GetInt("Level " + list.levelNum) == 1)
            {
                list.unLocked = 1;
            }
            //добавление кнопки
            LevelButton btn = tmpObject.transform.GetChild(2).GetComponent<LevelButton>();
            btn.levelText = list.nameLevel;
            btn.level = list.levelNum;

            if (list.unLocked == 1)
            {
                btn.transform.GetChild(1).GetComponent<UILabel>().text = unlockText;
            }
            else
            {
                btn.transform.GetChild(1).GetComponent<UILabel>().text = lockText;
                btn.GetComponent<UIButton>().enabled = false;
                btn.GetComponent<UIButtonOffset>().enabled = false;
                btn.GetComponent<UIButtonScale>().enabled = false;
                btn.GetComponent<UIButtonMessage>().enabled = false;
            }
        }  
        //сброс позиций
        space.GetComponent<UIGrid>().Reposition();
    }
}
