﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//Класс главного меню
public class MainMenu : MonoBehaviour
{
    #region Fields
    //Объект главного меню
    [SerializeField]
    private GameObject main;
    //Объект загрузки уровня
    [SerializeField]
    private GameObject loadLevel;
    //Объект загрузки рейтига
    [SerializeField]
    private GameObject rating;
    //Объект загрузки ввода имени
    [SerializeField]
    private GameObject username;
    //input ввода имени
    [SerializeField]
    private UIInput enterName;
    //массив меню
    private GameObject[] items;
    #endregion

    #region Init && Update
    void Start()
    {
        //активация ввода имени
        if (Params.username == null)
        {
            ResetAll();
            ActiveMenu(username);
            Sound.Instance.Initialize();
        }
        //переходы из попапы
        if (Params.menu == 3)
        {
            Home();
        }
        else if (Params.menu == 1)
        {
            LevelSelect();
        }
        else if (Params.menu == 2)
        {
            Rating();
        }
    }
    #endregion

    #region Action
    /// <summary>
    /// Переход на старт
    /// </summary>
    public void StartGame()
    {
        ActiveMenu(loadLevel);
    }
    /// <summary>
    /// Переход на выбор уровня
    /// </summary>
    public void LevelSelect()
    {
        ActiveMenu(loadLevel);
    }
    /// <summary>
    /// Переход на рейтинг
    /// </summary>
    public void Rating()
    {
        ActiveMenu(rating);
    }
    /// <summary>
    /// Выход из игры
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }
    /// <summary>
    /// Возврат
    /// </summary>
    public void Home()
    {
        ActiveMenu(main);
    }
    #endregion

    #region Functions
    /// <summary>
    /// Ввод имени
    /// </summary>
    public void EnterName()
    {
        if (enterName.text != string.Empty)
        {
            Params.username = enterName.text;

            ActiveMenu(main);
        }
    }
    /// <summary>
    /// Активация меню
    /// </summary>
    private void ActiveMenu(GameObject menu)
    {
        //use free version NGUI
        items = GameObject.FindGameObjectsWithTag("Menu");

        foreach (GameObject item in items)
        {
            NGUITools.SetActive(item, false);
        }
        NGUITools.SetActive(menu, true);
    }
    /// <summary>
    /// Сброс параметров
    /// </summary>
    private void ResetAll()
    {
        PlayerPrefs.DeleteAll();
        Params.Reset();
    }
    #endregion
}
