﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// Класс объекта рейтинга и сортировки
public class GameScore : IComparable<GameScore>
{
    public int ID { get; set; }
    public int Score { get; set; }
    public string Name { get; set; }
    public DateTime Date { get; set; }
    public int Level { get; set; }

    public GameScore(int id, string name, int score, DateTime date, int level)
    {
        this.Score = score;
        this.Name = name;
        this.Date = date;
        this.ID = id;
        this.Level = level;
    }
    /// <summary>
    /// Сортировка
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public int CompareTo(GameScore other)
    {

        if (other.Level < this.Level)
        {
            return -1;
        }
        else if (other.Level > this.Level)
        {
            return 1;
        }

        if (other.Score < this.Score)
        {
            return -1;
        }
        else if (other.Score > this.Score)
        {
            return 1;
        }
        else if (other.Date < this.Date)
        {
            return -1;
        }
        else if (other.Date > this.Date)
        {
            return 1;
        }
        return 0;
    }
}
