﻿using UnityEngine;
using System.Collections;
//Класс элемента рейтинга
public class ItemScore : MonoBehaviour 
{
    [SerializeField]
    private GameObject id;
    [SerializeField]
    private GameObject name;
    [SerializeField]
    private GameObject score;
    [SerializeField]
    private GameObject level;

    public void SetItem(int id,string name, int score, int level)
    {
        this.id.GetComponent<UILabel>().text = "#"+ id;
        this.name.GetComponent<UILabel>().text = name.ToString();
        this.score.GetComponent<UILabel>().text = score.ToString();
        this.level.GetComponent<UILabel>().text = level.ToString();
    }

}
