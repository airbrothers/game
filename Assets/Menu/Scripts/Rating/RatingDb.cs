﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;
//Класс рейтинга
public class RatingDb : MonoBehaviour
{
    private  Db db;

    private List<GameScore> gameScore = new List<GameScore>();

    [SerializeField]
    private GameObject space;

    [SerializeField]
    private GameObject scorePrefab;

	void Start () 
    {
        //коннект к БД
        db = Db.Instance;
        db.Initialize();

        ShowScores();
	}
    /// <summary>
    /// Получение показателей рейтинга
    /// </summary>
    private void GetScore()
    {
        IDataReader render = db.SelectSql("SELECT * FROM Score");
        if (render != null)
        {
            while (render.Read())
            {
                gameScore.Add(new GameScore(
                         render.GetInt32(0),
                         render.GetString(1),
                         render.GetInt32(2),
                         render.GetDateTime(3),
                         render.GetInt32(4)
                     ));
            }
            db.CloseConnect();
            gameScore.Sort();
        }
    }
    /// <summary>
    /// Вывод показателей рейтинга
    /// </summary>
    public void ShowScores()
    {
        GetScore();

        int index = 0;
        foreach (GameScore score in gameScore)
        {
            GameObject tmpObject = (GameObject)Instantiate(scorePrefab);
            tmpObject.transform.parent = space.transform;
            tmpObject.transform.localScale = Vector3.one;
            tmpObject.GetComponent<ItemScore>().SetItem(++index, score.Name.ToString(), score.Score, score.Level);
            space.GetComponent<UIGrid>().Reposition();
    
        }
    }
    /// <summary>
    /// Запись прохождения уровня
    /// </summary>
    /// <param name="name"></param>
    /// <param name="score"></param>
    /// <param name="level"></param>
    private void InsertScore(string name, int score, int level)
    {
        string sqlQuery = String.Format("INSERT INTO Score(Name,Score,Level) VALUES (\"{0}\",\"{1}\",\"{2}\")", name, score, level);
        db.ActionSql(sqlQuery);
    }
    /// <summary>
    /// Удаление записи рейтинга
    /// </summary>
    /// <param name="id"></param>
    private void DeleteScore(int id)
    {
        string sqlQuery = String.Format("DELETE FROM Score Where PlayerID = \"{0}\"", id);
        db.ActionSql(sqlQuery);
    }

    /// <summary>
    /// Запись или обновление баллов
    /// </summary>
    /// <param name="point"></param>
    public static void Add(float point)
    {
        if (Params.username != null)
        {
            Db db = Db.Instance;

            db.Initialize();

            string sqlQueryId = String.Format("SELECT ID FROM SCORE WHERE Name = '{0}' AND Level =  '{1}' ", Params.username, Params.level);
            IDataReader render = db.SelectSql(sqlQueryId);

            string sqlQuery;
            if (render.Read() != false)
            {
                sqlQuery = String.Format("UPDATE Score SET score =\"{0}\" WHERE id = '{1}'", point, render.GetInt32(0));
            }
            else
            {
                sqlQuery = String.Format("INSERT INTO Score(Name,Score,Level) VALUES ('{0}','{1}','{2}')", Params.username, point, Params.level);
            }

            db.CloseConnect();

            db.ActionSql(sqlQuery);
        }
    }
}
