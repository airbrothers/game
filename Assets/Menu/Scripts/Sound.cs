﻿using UnityEngine;
using System.Collections;

public class Sound : MonoBehaviour
{
    //объект аудио
    private AudioSource theSource;

    private static Sound instance = null;
    // <summary>
    // создание сущности
    // </summary>
    public static Sound Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("Sound").AddComponent<Sound>();
            }
            return instance;
        }
    }
    public void OnApplicationQuit()
    {
        DestroyInstance();
    }

    public void DestroyInstance()
    {
        instance = null;
    }

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    // <summary>
    // Воспроизведение аудио
    // </summary>
    // <param name="audio"></param>
    public void Initialize()
    {
        if (transform.gameObject.GetComponent<AudioSource>())
        {
            theSource = transform.gameObject.GetComponent<AudioSource>();
        }
        else
        {
            theSource = transform.gameObject.AddComponent<AudioSource>();
        }

        AudioClip mainAudio = Resources.Load<AudioClip>("main");

        if (mainAudio != null)
        {
            theSource.clip = mainAudio;
            theSource.volume = 0.1f;
            theSource.playOnAwake = true;
            theSource.Play();
        }
        else
        {
            Debug.LogWarning("Нет звука");
        }
    }

}
