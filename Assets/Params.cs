﻿using System;
using System.Collections;

public static class Params
{
    public static int menu;
    public static int level;
    public static int maxLevel;
    public static int nextLevel;
    public static string username;

    public static bool camera2D = true;
    public static bool camera3D = false;

    public static bool lose = false;
    public static bool win = false;
    public static bool pause = false;
    /// <summary>
    /// Сброс параметров попапа
    /// </summary>
    public static void Reset()
    {
        lose = win = pause = false;
    }
    /// <summary>
    /// Полочения нового уровня
    /// </summary>
    /// <returns></returns>
    public static int GetNextLevel()
    {
        if (level < maxLevel)
        {
            return ++level;
        }
        return 0;
    }
    /// <summary>
    /// Переключение камеры
    /// </summary>
    public static void SwitchCamera()
    {
        camera2D = !camera2D;
        camera3D = !camera3D;
    }
    /// <summary>
    /// Провекра на активность
    /// </summary>
    /// <returns></returns>
    public static bool IsActivePopup()
    {
        return (lose || win || pause) ? true : false;
    }


}
